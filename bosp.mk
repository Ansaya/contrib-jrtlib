
ifdef CONFIG_CONTRIB_TESTING_JTESTAPP

# Targets provided by this project
.PHONY: rtlib_jtestapp clean_rtlib_jtestapp

# Add this to the "contrib_testing" target
testing: rtlib_jtestapp
clean_testing: clean_rtlib_jtestapp

MODULE_CONTRIB_TESTING_JTESTAPP=contrib/testing/rtlib-jtestapp

rtlib_jtestapp: external
	@echo
	@echo "====      Building RTLib Java Test Application       ===="
	@echo " Using JDK    : $(Java_VERSION_STRING)"
	@echo " Sysroot      : $(PLATFORM_SYSROOT)"
	@echo " BOSP Options : $(CMAKE_COMMON_OPTIONS)"
	@[ -d $(MODULE_CONTRIB_TESTING_JTESTAPP)/build/$(BUILD_TYPE) ] || \
		mkdir -p $(MODULE_CONTRIB_TESTING_JTESTAPP)/build/$(BUILD_TYPE) || \
		exit 1
	@cd $(MODULE_CONTRIB_TESTING_JTESTAPP)/build/$(BUILD_TYPE) && \
		cmake $(CMAKE_COMMON_OPTIONS) ../.. || \
		exit 1
	@cd $(MODULE_CONTRIB_TESTING_JTESTAPP)/build/$(BUILD_TYPE) && \
		make -j$(CPUS) install || \
		exit 1

clean_rtlib_jtestapp:
	@echo
	@echo "== Clean-up RTLib Java Test Application ="
	@[ ! -f $(BUILD_DIR)/usr/java/bbque-jtestapp.jar ] || \
		rm -f $(BUILD_DIR)/etc/bbque/recipes/BbqRTLibJTestApp*; \
		rm -f $(BUILD_DIR)/usr/bin/bbque-jtestapp*
	@rm -rf $(MODULE_CONTRIB_TESTING_JTESTAPP)/build
	@echo

else # CONFIG_CONTRIB_TESTING_JTESTAPP

rtlib_jtestapp:
	$(warning contrib JTestApp module disabled by BOSP configuration)
	$(error BOSP compilation failed)

endif # CONFIG_CONTRIB_TESTING_JTESTAPP
