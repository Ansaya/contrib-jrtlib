package bbque;

import bbque.RTLib.*;
import static bbque.RTLib.RTLibExitCode.*;
import static bbque.RTLib.RTLibResourceType.*;

public class TestWorkload extends BbqueEXC {

    private final long _cycle_ms;

    private final long _cycles_total;

    private long _cycle_length;

    public TestWorkload(
        String name, 
        String recipePath,
        long cycle_ms,
        long cycles_total) throws RTLibException {
        super(name, recipePath);

        this._cycle_ms = cycle_ms;
        this._cycles_total = cycles_total;

        System.out.format("New TestWorkload with: cycle time %d[ms], cycles count %d%n",
            cycle_ms, cycles_total);

        System.out.println("EXC Unique IDentifier (UID): " + GetUniqueID());
    }

    protected int onSetup() {
        System.out.println("TestWorkload::onSetup():");

        return OK.ordinal();
    }

    protected int onConfigure(int awm_id) {
        System.out.format("TestWorkload::onConfigure(): EXC [%s], AWM [%02d]%n",
            _name, awm_id);

        int proc_quota = 0, proc_nr = -1, mem = 0;
        
        try {
            proc_quota = GetAssignedResources(PROC_ELEMENT);
            proc_nr = GetAssignedResources(PROC_NR);
            mem = GetAssignedResources(MEMORY);
        } catch(RTLibException rte) {
            rte.printStackTrace();
        }

        System.out.format("TestWorkload::onConfigure(): EXC [%s], AWM [%02d]"
            + " => R<proc_quota>=%3d, R<PROC_nr>=%2d, R<MEM>=%3d%n",
            _name, awm_id, proc_quota, proc_nr, mem);

        if(proc_nr == -1)
            proc_nr = 1;

        _cycle_length = 1000 * _cycle_ms * (4 / proc_nr);

        System.out.format("TestWorkload::onConfigure(): EXC [%s], AWM [%02d]"
            + " => cucle time %d[ms]%n", _name, awm_id, _cycle_length);

        try {
            SetMinimumCycleTimeUs(_cycle_length);
        }catch(RTLibException rte) {
            rte.printStackTrace();
        }
        
        return OK.ordinal();
    }

    protected int onSuspend() {
        System.out.println("TestWorkload::onSuspend():");

        return 0;
    }

    protected int onResume() {
        System.out.println("TestWorkload::onResume():");

        return 0;
    }

    protected int onRun() {
        short currentAWM;
        
        if(Cycles() >= _cycles_total)
            return EXC_WORKLOAD_NONE.ordinal();

        System.out.format("TestWorkload::onRun(): EXC [%s], AWM [%02d]"
            + " => processing %d[ms]%n", _name, CurrentAWM(), _cycle_length / 1000);

        return OK.ordinal();
    }

    protected int onMonitor() {

        RTLibWorkingModeParams wmp = null;
        
        try {
            wmp = WorkingModeParams();
        } catch(RTLibException rte) {}

        if(wmp != null)
            System.out.format("TestWorkload::onMonitor(): EXC [%s], AWM [%02d]"
                + " => cycles [%d/%d], CPS = %.2f%n", _name, wmp.CurrentAWM(), Cycles(), _cycles_total, GetCPS());
        else
            System.out.format("TestWorkload::onMonitor(): EXC [%s], AWM [%02d]"
                + " => cycles [%d/%d], CPS = %.2f%n", _name, CurrentAWM(), Cycles(), _cycles_total, GetCPS());

        return OK.ordinal();
    }

    protected int onRelease() {
        System.out.println("TestWorkload::onRelease():");

        return 0;
    }

}
