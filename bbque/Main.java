package bbque;

import bbque.RTLib.*;

import java.util.List;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;
import java.io.IOException;
import java.nio.file.Paths;

public class Main {

    private static String _recipe = "BbqRTLibTestApp";

    private static String _excBaseName = "exc";

    private static int _exc_count = 1;

    private static int _workload_time = 1;

    private static int _cycles_count = 5;

    private static List<TestWorkload> _test_workloads = new ArrayList<TestWorkload>();

    private static void printHelp() {
        System.out.println("JTestApp Configuration Options:");
        System.out.println("-h                          print this help message");
        System.out.format("-r arg (=%s)%n", _recipe);
        System.out.println("                            recipe name (for all EXCs)");
        System.out.format("-e arg (=%d)                 number of EXC to spawm%n", _exc_count);
        System.out.format("-w arg (=%d)                 overall BCE time [s]%n", _workload_time);
        System.out.format("-c arg (=%d)                 workload cycles count%n", _cycles_count);
    }

    private static boolean parseInput(String[] args) {
        
        final Map<String, List<String>> params = new HashMap<>();

        List<String> options = null;
        for (int i = 0; i < args.length; i++) {
            final String a = args[i];

            if (a.charAt(0) == '-') {
                if (a.length() < 2) {
                    return false;
                }

                options = new ArrayList<>();
                params.put(a.substring(1), options);
            }
            else if (options != null) {
                options.add(a);
            }
            else {
                return false;
            }
        }

        if(params.containsKey("h")) return false;

        if(params.containsKey("r")) _recipe = params.get("r").get(0);

        if(params.containsKey("e") && !params.get("e").isEmpty())
            _exc_count = Integer.parseInt(params.get("e").get(0));

        if(params.containsKey("w") && !params.get("w").isEmpty())
            _workload_time = Integer.parseInt(params.get("w").get(0));
        
        if(params.containsKey("c") && !params.get("c").isEmpty())
            _cycles_count = Integer.parseInt(params.get("c").get(0));
        
        return true;
    }

    private static void SetupEXCs(int exc_count, String recipe) throws RTLibException {
        System.out.format("STEP 1. Registering [%03d] EXCs, using recipe [%s]..%n",
            exc_count, recipe);

        int cycle_time = _workload_time * 1000 / _cycles_count;

        for(int i = 0; i < exc_count; i++) {

            String current_name = _excBaseName + String.format("%2d", i);

            TestWorkload pexc = new TestWorkload(
                current_name, recipe, cycle_time, _cycles_count);

            if(pexc.isRegistered())
                _test_workloads.add(pexc);

        }

        System.out.format("STEP 3. Starting [%03d] EXCs control threads...%n",
            _test_workloads.size());

        for(TestWorkload it : _test_workloads)
            it.Start();
    }

    private static void RunEXCs() throws RTLibException {
        System.out.format("STEP 4. Running [%03d] control threads...%n", _test_workloads.size());

        for(TestWorkload it : _test_workloads)
            it.WaitCompletion();
    }

    private static void DestroyEXCs() {
        for(TestWorkload it : _test_workloads)
            it.Dispose();

        _test_workloads.clear();
    }

    public static void main(String[] args) throws InterruptedException {

        if(!parseInput(args)) {
            printHelp();
            return;
        }

        System.out.println("STEP 0. Initializing RTLib library, application [JavaTestApp]...");
        try {
            RTLib.Init("JavaTestApp");
        }
        catch(RTLibException rte) {
            System.out.println("Error during rtlib initialization.");
            rte.printStackTrace();

            System.exit(-1);
        }    
        
        try {

            SetupEXCs(_exc_count, _recipe);

            RunEXCs();

        }
        catch(RTLibException rte) {
            rte.printStackTrace();

            System.exit(-1);
        }

        DestroyEXCs();

        System.out.println("===== RTLibJTestApp DONE! =====");

    }
}

